*SPSStaticAssetHack*

This package provides an exension to silence Xcode warnings regarding non-Sendable static assests. The warnings are generated in code generated by the compiler/Xcode and will require Apple to correct the issue. This pacakge is a workaround until Apple resolves the issues.

Hat tip to: @markbattistella@mastodon.au (Mastodon Link) for pointing out this workaround
