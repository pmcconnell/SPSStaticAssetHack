import SwiftUI

// these are used to suppress Xcode warnings until macros are updated for
// processing static resources

@available(iOS 17.0, *)
extension DeveloperToolsSupport.ColorResource: @unchecked Sendable {}

@available(iOS 17.0, *)
extension DeveloperToolsSupport.ImageResource: @unchecked Sendable {}

